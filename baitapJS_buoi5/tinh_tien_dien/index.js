function tinhTienDien() {
  console.log("yes");
  var soKw = document.getElementById("kw").value * 1;
  var result = document.getElementById("result");
  var tienDien = 0;

  if (soKw <= 50) {
    tienDien = 500 * soKw;
  } else if (soKw <= 100) {
    tienDien = 500 * 50 + 650 * (soKw - 50);
  } else if (soKw <= 200) {
    tienDien = 500 * 50 + 650 * 50 + (soKw - 100) * 850;
  } else if (soKw <= 350) {
    tienDien = 500 * 50 + 650 * 50 + 100 * 850 + (soKw - 200) * 1100;
  } else {
    tienDien =
      500 * 50 + 650 * 50 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
  }

  result.innerText = `Tiền Điện Phải Trả Là: ${tienDien}`;
}
